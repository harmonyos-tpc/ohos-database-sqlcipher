# ohos-database-sqlcipher

ohos-database-sqlcipher is used for full database encryption.

# Usage Instructions

1. A sample project which provides runnable code examples that demonstrate uses of the classes in this project is available in the sample/ folder.
2. Create a contract class like below, mentioning the column names

```
public final class SqlcipherContract {
    public SqlcipherContract() {}
    public static abstract class SqlEntry implements BaseColumns {
        public static final String TABLE_NAME = "news";
        public static final String COLUMN_NAME_ENTRY_ID = "news_id";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_SUBTITLE = "subtitle";
    }
}
```

3)Create a database helper class which extends the sqlcipher SQLiteOpenHelper and uses SQLiteDatabase classes as below.Also mention the absolute path of database.

```
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;
public class SqlcipherDbHelper extends SQLiteOpenHelper {
```

4)In the MainAbility class, add the below statement, by passing any random passphrase

``` 
SQLiteDatabase db = SqlcipherDbHelper.getInstance(this).getWritableDatabase("passphrase");
```

# Installation Instructions:

1. For using ohos-database-sqlcipher module in your sample application, add below dependencies:
    ```
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation project(':ohos-database-sqlcipher')
    ```

2. For using ohos-database-sqlcipher in separate application, add the below dependencies and include "ohos-gif-drawable.har" in libs folder of "entry" module:
    ```
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation files('libs/ohos-database-sqlcipher.har')
    ```

    Download the har file (https://s01.oss.sonatype.org/content/repositories/releases/io/openharmony/tpc/thirdlib/ohos-database-sqlcipher/1.0.2/ohos-database-sqlcipher-1.0.2.har ), extract the libsqlcipher.so file and add in /libs/arm64-v8a (Create the folder if not present) of entry app in both cases

3. For using ohos-database-sqlcipher from a remote repository in separate application, add the below dependencies in build.gradle of "entry" module:
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation 'io.openharmony.tpc.thirdlib:ohos-database-sqlcipher:1.0.2'
}
```

# License

The library is licensed under Apache 2.0. The SQLCipher code itself is licensed under a BSD-style license from Zetetic LLC. Finally, the original SQLite code itself is in the public domain.

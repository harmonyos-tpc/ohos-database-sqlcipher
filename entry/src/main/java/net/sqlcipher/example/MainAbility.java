/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sqlcipher.example;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.example.slice.MainAbilitySlice;

/**
 * MainAbility
 */
public class MainAbility extends Ability {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x002B, "MainAbility");

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        SQLiteDatabase.loadLibs(this);
        insertSthToDb();
    }


    private void insertSthToDb() {
        SQLiteDatabase db = SqlcipherDbHelper.getInstance(this).getWritableDatabase("somePass");
        ValuesBucket values = new ValuesBucket();
        values.putInteger(SqlcipherContract.SqlEntry.COLUMN_NAME_ENTRY_ID, 1);
        values.putString(SqlcipherContract.SqlEntry.COLUMN_NAME_TITLE, "Easter Bunny has escaped!");
        values.putString(SqlcipherContract.SqlEntry.COLUMN_NAME_SUBTITLE, "A thrilling story which proves how " +
                "fragile our hearts are...");

        db.insert(SqlcipherContract.SqlEntry.TABLE_NAME, null, values);

        ResultSet cursor = db.rawQuery("SELECT * FROM '" + SqlcipherContract.SqlEntry.TABLE_NAME + "';", null);
        if (cursor != null) {
            if (cursor.goToFirstRow()) {
                HiLog.debug(LABEL, "Rows index: " + cursor.getRowIndex()); /* modify to Hilog.error to view */
                HiLog.debug(LABEL, "entry id: " + cursor.getString(1));
                HiLog.debug(LABEL, "title: " + cursor.getString(2));
                HiLog.debug(LABEL, "subtitle: " + cursor.getString(3));
            }
        }
        cursor.close();
        db.close();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}

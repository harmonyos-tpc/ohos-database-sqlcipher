/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sqlcipher.example;

import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

/**
 * SqlcipherDbHelper
 */
public class SqlcipherDbHelper extends SQLiteOpenHelper {
    /**
     * DATABASE_NAME
     */
    public static final String DATABASE_NAME = "/data/user/0/net.sqlcipher.example/databases/FeedReader.db";
    /**
     * DATABASE_VERSION
     */
    public static final int DATABASE_VERSION = 1;

    private static final String TEXT_TYPE = " TEXT";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + SqlcipherContract.SqlEntry.TABLE_NAME + " (" +
                    SqlcipherContract.SqlEntry.ID + " INTEGER PRIMARY KEY," +
                    SqlcipherContract.SqlEntry.COLUMN_NAME_ENTRY_ID + TEXT_TYPE + "," +
                    SqlcipherContract.SqlEntry.COLUMN_NAME_TITLE + TEXT_TYPE + "," +
                    SqlcipherContract.SqlEntry.COLUMN_NAME_SUBTITLE + TEXT_TYPE +
                    " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + SqlcipherContract.SqlEntry.TABLE_NAME;

    public SqlcipherDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private static SqlcipherDbHelper instance;
    /**
     * onConfigureCalled flag
     */
    public boolean onConfigureCalled = false;

    /**
     * returns instance
     * @param context context
     * @return SqlcipherDbHelper instance
     */
    public static synchronized SqlcipherDbHelper getInstance(Context context) {
        if (instance == null) {
            instance = new SqlcipherDbHelper(context);
        }
        return instance;
    }

    /**
     * onCreate
     * @param db The database.
     */
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    /**
     * onUpgrade
     * @param db The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    /**
     * onConfigure
     * @param database database
     */
    public void onConfigure(SQLiteDatabase database) {
        onConfigureCalled = true;
    }
}

/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sqlcipher.example;

/**
 * SqlcipherContract
 */
public final class SqlcipherContract {
    public SqlcipherContract() {}

    /**
     * Inner class that defines the table contents
     */
    public abstract static class SqlEntry implements BaseColumns {
        /**
         * TABLE_NAME
         */
        public static final String TABLE_NAME = "news";
        /**
         * COLUMN_NAME_ENTRY_ID
         */
        public static final String COLUMN_NAME_ENTRY_ID = "news_id";
        /**
         * COLUMN_NAME_TITLE
         */
        public static final String COLUMN_NAME_TITLE = "title";
        /**
         * COLUMN_NAME_SUBTITLE
         */
        public static final String COLUMN_NAME_SUBTITLE = "subtitle";
    }
}

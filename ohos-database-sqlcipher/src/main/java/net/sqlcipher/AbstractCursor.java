/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sqlcipher;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.data.rdb.DataObservable;
import ohos.data.rdb.DataObserver;
import ohos.data.resultset.ResultSet;
import ohos.data.resultset.SharedBlock;
import ohos.aafwk.ability.IDataAbilityObserver;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.PacMap;
import ohos.utils.net.Uri;


/**
 * This is an abstract cursor class that handles a lot of the common code
 * that all cursors need to deal with and is provided for convenience reasons.
 */
public abstract class AbstractCursor implements ohos.data.resultset.SharedResultSet, ohos.data.resultset.ResultSet {
    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x002E,"AbstractCursor");

      DataObservable mDataSetObservable = new DataObservable();
     // ContentObservable mContentObservable = new ContentObservable();

    private PacMap mExtras = PacMap.EMPTY_PAC_MAP;

    /* -------------------------------------------------------- */
    /* These need to be implemented by subclasses */
    abstract public int getRowCount();

    abstract public String[] getAllColumnNames();

    abstract public String getString(int column);
    abstract public short getShort(int column);
    abstract public int getInt(int column);
    abstract public long getLong(int column);
    abstract public float getFloat(int column);
    abstract public double getDouble(int column);
    abstract public boolean isColumnNull(int column);

    abstract public ResultSet.ColumnType getColumnTypeForIndex(int column);

    // TODO implement getBlob in all cursor types
    public byte[] getBlob(int column) {
        throw new UnsupportedOperationException("getBlob is not supported");
    }
    /* -------------------------------------------------------- */
    /* Methods that may optionally be implemented by subclasses */

    /**
     * returns a pre-filled window, return NULL if no such window
     * @return SharedBlock
     */
    public SharedBlock getBlock() {
        return null;
    }

    public int getColumnCount() {
        return getAllColumnNames().length;
    }

    public void deactivate() {
        deactivateInternal();
    }

    /**
     * deactivateInternal
     * @hide
     */
    public void deactivateInternal() {
        if (mSelfObserver != null) {
            mContentResolver.unregisterObserver(mNotifyUri, mSelfObserver);
            mSelfObserverRegistered = false;
        }
        mDataSetObservable.notifyObservers(); //notifyInvalidated not available
    }

    public boolean requery() {
        if (mSelfObserver != null && mSelfObserverRegistered == false) {

            mContentResolver.registerObserver(mNotifyUri, mSelfObserver);
            mSelfObserverRegistered = true;
        }
        mDataSetObservable.notifyObservers();
        return true;
    }

    public boolean isClosed() {
        return mClosed;
    }

    public void close() {
        mClosed = true;
   //     mContentObservable.unregisterAll(); //todo
        deactivateInternal();
    }

    /**
     * commitUpdates
     * @hide
     * @deprecated
     * @param values value
     * @return true/false
     */
    public boolean commitUpdates(Map<? extends Long,? extends Map<String,Object>> values) {
        return false;
    }

    /**
     * deleteRow
     * @hide
     * @deprecated
     * @return true/false
     */
    public boolean deleteRow() {
        return false;
    }

    /**
     * This function is called every time the cursor is successfully scrolled
     * to a new position, giving the subclass a chance to update any state it
     * may have. If it returns false the move function will also do so and the
     * cursor will scroll to the beforeFirst position.
     *
     * @param oldPosition the position that we're moving from
     * @param newPosition the position that we're moving to
     * @return true if the move is successful, false otherwise
     */
    public boolean onGo(int oldPosition, int newPosition) {
        return true;
    }


    public void copyStringToBuffer(int columnIndex, CharArrayBuffer buffer) {
        // Default implementation, uses getString
        String result = getString(columnIndex);
        if (result != null) {
            char[] data = buffer.data;
            if (data == null || data.length < result.length()) {
                buffer.data = result.toCharArray();
            } else {
                result.getChars(0, result.length(), data, 0);
            }
            buffer.sizeCopied = result.length();
        } else {
            buffer.sizeCopied = 0;
        }
    }

    /* -------------------------------------------------------- */
    /* Implementation */
    public AbstractCursor() {
        mPos = -1;
        mRowIdColumnIndex = -1;
        mCurrentRowID = null;
        mUpdatedRows = new HashMap<Long, Map<String, Object>>();
    }

    public final int getRowIndex() {
        return mPos;
    }

    public final boolean goToRow(int position) {
        // Make sure position isn't past the end of the cursor
        final int count = getRowCount();
        if (position >= count) {
            mPos = count;
            return false;
        }

        // Make sure position isn't before the beginning of the cursor
        if (position < 0) {
            mPos = -1;
            return false;
        }

        // Check for no-op moves, and skip the rest of the work for them
        if (position == mPos) {
            return true;
        }

        boolean result = onGo(mPos, position);
        if (result == false) {
            mPos = -1;
        } else {
            mPos = position;
            if (mRowIdColumnIndex != -1) {
                mCurrentRowID = Long.valueOf(getLong(mRowIdColumnIndex));
            }
        }

        return result;
    }

    /**
     * Copy data from cursor to CursorWindow
     * @param position start position of data
     * @param window
     */
    public void fillBlock(int position, ohos.data.resultset.SharedBlock window) {
        DatabaseUtils.cursorFillWindow(this, position, window);
    }

    public final boolean goTo(int offset) {
        return goToRow(mPos + offset);
    }

    public final boolean goToFirstRow() {
        return goToRow(0);
    }

    public final boolean goToLastRow() {
        return goToRow(getRowCount() - 1);
    }

    public final boolean goToNextRow() {
        return goToRow(mPos + 1);
    }

    public final boolean goToPreviousRow() {
        return goToRow(mPos - 1);
    }

    public final boolean isAtFirstRow() {
        return mPos == 0 && getRowCount() != 0;
    }

    public final boolean isAtLastRow() {
        int cnt = getRowCount();
        return mPos == (cnt - 1) && cnt != 0;
    }

    public final boolean isStarted() {
        if (getRowCount() == 0) {
            return true;
        }
        return mPos == -1;
    }

    public final boolean isEnded() {
        if (getRowCount() == 0) {
            return true;
        }
        return mPos == getRowCount();
    }

    public int getColumnIndexForName(String columnName) {
        // Hack according to bug 903852
        final int periodIndex = columnName.lastIndexOf('.');
        if (periodIndex != -1) {
            Exception e = new Exception();
            HiLog.error(label, "requesting column name with table name -- " + columnName, e);
            columnName = columnName.substring(periodIndex + 1);
        }

        String columnNames[] = getAllColumnNames();
        int length = columnNames.length;
        for (int i = 0; i < length; i++) {
            if (columnNames[i].equalsIgnoreCase(columnName)) {
                return i;
            }
        }

        /*if (Config.LOGV) {*/ //todo
            if (getRowCount() > 0) {
                HiLog.warn(label,"AbstractCursor", "Unknown column " + columnName);
            }
       //
        return -1;
    }

    public String getColumnNameForIndex(int columnIndex) {
        return getAllColumnNames()[columnIndex];
    }

    /**
     * updateBlob
     * @hide
     * @deprecated
     * @param columnIndex index
     * @param value value
     * @return true/false
     */
    public boolean updateBlob(int columnIndex, byte[] value) {
        return update(columnIndex, value);
    }

    /**
     * updateString
     * @hide
     * @deprecated
     * @param columnIndex index
     * @param value value
     * @return true/false
     */
    public boolean updateString(int columnIndex, String value) {
        return update(columnIndex, value);
    }

    /**
     * updateShort
     * @hide
     * @deprecated
     * @param columnIndex index
     * @param value value
     * @return true/false
     */
    public boolean updateShort(int columnIndex, short value) {
        return update(columnIndex, Short.valueOf(value));
    }

    /**
     * updateInt
     * @hide
     * @deprecated
     * @param columnIndex index
     * @param value value
     * @return true/false
     */
    public boolean updateInt(int columnIndex, int value) {
        return update(columnIndex, Integer.valueOf(value));
    }

    /**
     * updateLong
     * @hide
     * @deprecated
     * @param columnIndex index
     * @param value value
     * @return true/false
     */
    public boolean updateLong(int columnIndex, long value) {
        return update(columnIndex, Long.valueOf(value));
    }

    /**
     * updateFloat
     * @hide
     * @deprecated
     * @param columnIndex index
     * @param value value
     * @return true/false
     */
    public boolean updateFloat(int columnIndex, float value) {
        return update(columnIndex, Float.valueOf(value));
    }

    /**
     * updateDouble
     * @hide
     * @deprecated
     * @param columnIndex index
     * @param value value
     * @return true/false
     */
    public boolean updateDouble(int columnIndex, double value) {
        return update(columnIndex, Double.valueOf(value));
    }

    /**
     * updateToNull
     * @hide
     * @deprecated
     * @param columnIndex index
     * @return true/false
     */
    public boolean updateToNull(int columnIndex) {
        return update(columnIndex, null);
    }

    /**
     * update
     * @hide
     * @deprecated
     * @param columnIndex index
     * @param obj object
     * @return true/false
     */
    public boolean update(int columnIndex, Object obj) {
        if (!supportsUpdates()) {
            return false;
        }

        // Long.valueOf() returns null sometimes!
//        Long rowid = Long.valueOf(getLong(mRowIdColumnIndex));
        Long rowid = Long.valueOf(getLong(mRowIdColumnIndex));
        if (rowid == null) {
            throw new IllegalStateException("null rowid. mRowIdColumnIndex = " + mRowIdColumnIndex);
        }

        synchronized(mUpdatedRows) {
            Map<String, Object> row = mUpdatedRows.get(rowid);
            if (row == null) {
                row = new HashMap<String, Object>();
                mUpdatedRows.put(rowid, row);
            }
            row.put(getAllColumnNames()[columnIndex], obj);
        }

        return true;
    }

    /**
     * Returns <code>true</code> if there are pending updates that have not yet been committed.
     *
     * @return <code>true</code> if there are pending updates that have not yet been committed.
     * @hide
     * @deprecated
     */
    public boolean hasUpdates() {
        synchronized(mUpdatedRows) {
            return mUpdatedRows.size() > 0;
        }
    }

    /**
     * abortUpdates
     * @hide
     * @deprecated
     */
    public void abortUpdates() {
        synchronized(mUpdatedRows) {
            mUpdatedRows.clear();
        }
    }

    /**
     * commitUpdates
     * @hide
     * @deprecated
     * @return true/false
     */
    public boolean commitUpdates() {
        return commitUpdates(null);
    }

    /**
     * supportsUpdates
     * @hide
     * @deprecated
     * @return true/false
     */
    public boolean supportsUpdates() {
        return mRowIdColumnIndex != -1;
    }

    public void registerObserver(DataObserver observer) {
   //     mContentObservable.registerObserver(observer); //todo contentobservable not available
    }

    public void unregisterObserver(DataObserver observer) {
        // cursor will unregister all observers when it close
        if (!mClosed) {
    //        mContentObservable.unregisterObserver(observer);//todo
        }
    }

    /**
     * This is hidden until the data set change model has been re-evaluated.
     * @hide
     */
    protected void notifyDataSetChange() {
        mDataSetObservable.notifyObservers();
    }

    /**
     * This is hidden until the data set change model has been re-evaluated.
     * @hide
     * @return DataObservable
     */
    protected DataObservable getDataSetObservable() {
        return mDataSetObservable;

    }
    /*public void registerObserver(DataObserver observer) { //added instead of datasetobserver
   //     mDataSetObservable.registerObserver(observer); //todo
    }

    public void unregisterObserver(DataObserver observer) {
    //    mDataSetObservable.unregisterObserver(observer);//todo
    }*/

    /**
     * Subclasses must call this method when they finish committing updates to notify all
     * observers.
     *
     * @param selfChange
     */
    protected void onChange(boolean selfChange) {
        synchronized (mSelfObserverLock) {
     //       mContentObservable.dispatchChange(selfChange); //todo
            if (mNotifyUri != null && selfChange) {
                mContentResolver.notifyChange(mNotifyUri);
            }
        }
    }

    /**
     * Specifies a content URI to watch for changes.
     *
     * @param cr The content resolver from the caller's context.
     * @param notifyUri The URI to watch for changes. This can be a
     * specific row URI, or a base URI for a whole class of content.
     */
    @Override
    public void setAffectedByUris(Object cr, List <Uri> notifyUri) {
        if (cr instanceof DataAbilityHelper) {
            synchronized (mSelfObserverLock) {
                mNotifyUriList = notifyUri;
                for(Uri uri:notifyUri) {
                    mContentResolver = (DataAbilityHelper) cr;
                    if (mSelfObserver != null) {
                        mContentResolver.unregisterObserver(uri, mSelfObserver);
                    }
                    mSelfObserver = new SelfContentObserver(this);
                    mContentResolver.registerObserver(mNotifyUri, mSelfObserver);
                    mSelfObserverRegistered = true;
                }
            }
        }
    }

    public List<Uri> getAffectedByUris() {
        return mNotifyUriList;
    }

    public boolean getWantsAllOnMoveCalls() {
        return false;
    }

    public void setExtensions(PacMap extras) {
        mExtras = (extras == null) ? PacMap.EMPTY_PAC_MAP : extras;
    }

    public PacMap getExtensions() {
        return mExtras;
    } //todo

    public PacMap respond(PacMap extras) {
        return PacMap.EMPTY_PAC_MAP;
    }  // no such method in hmos

    /**
     * This function returns true if the field has been updated and is
     * used in conjunction with {@link #getUpdatedField} to allow subclasses to
     * support reading uncommitted updates. NOTE: This function and
     * {@link #getUpdatedField} should be called together inside of a
     * block synchronized on mUpdatedRows.
     *
     * @param columnIndex the column index of the field to check
     * @return true if the field has been updated, false otherwise
     */
    protected boolean isFieldUpdated(int columnIndex) {
        if (mRowIdColumnIndex != -1 && mUpdatedRows.size() > 0) {
            Map<String, Object> updates = mUpdatedRows.get(mCurrentRowID);
            if (updates != null && updates.containsKey(getAllColumnNames()[columnIndex])) {
                return true;
            }
        }
        return false;
    }

    /**
     * This function returns the uncommitted updated value for the field
     * at columnIndex.  NOTE: This function and {@link #isFieldUpdated} should
     * be called together inside of a block synchronized on mUpdatedRows.
     *
     * @param columnIndex the column index of the field to retrieve
     * @return the updated value
     */
    protected Object getUpdatedField(int columnIndex) {
        Map<String, Object> updates = mUpdatedRows.get(mCurrentRowID);
        return updates.get(getAllColumnNames()[columnIndex]);
    }

    /**
     * This function throws CursorIndexOutOfBoundsException if
     * the cursor position is out of bounds. Subclass implementations of
     * the get functions should call this before attempting
     * to retrieve data.
     *
     * @throws CursorIndexOutOfBoundsException
     */
    protected void checkPosition() {
        if (-1 == mPos || getRowCount() == mPos) {
            throw new CursorIndexOutOfBoundsException(mPos, getRowCount());
        }
    }

    @Override
    protected void finalize() {
        if (mSelfObserver != null && mSelfObserverRegistered == true) {
            mContentResolver.unregisterObserver(mNotifyUri, mSelfObserver);
        }
    }

    /**
     * Cursors use this class to track changes others make to their URI.
     */
    protected static class SelfContentObserver implements IDataAbilityObserver {
        WeakReference<AbstractCursor> mCursor;

        public SelfContentObserver(AbstractCursor cursor) {
           // super(null); //no superclass
            mCursor = new WeakReference<AbstractCursor>(cursor);
        }

        /*@Override
        public boolean deliverSelfNotifications() {
            return false;
        }*/

        @Override
        public void onChange() {
            AbstractCursor cursor = mCursor.get();
            if (cursor != null) {
                cursor.onChange(false);
            }
        }

    }

    /**
     * This HashMap contains a mapping from Long rowIDs to another Map
     * that maps from String column names to new values. A NULL value means to
     * remove an existing value, and all numeric values are in their class
     * forms, i.e. Integer, Long, Float, etc.
     */
    protected HashMap<Long, Map<String, Object>> mUpdatedRows;

    /**
     * This must be set to the index of the row ID column by any
     * subclass that wishes to support updates.
     */
    protected int mRowIdColumnIndex;

    protected int mPos;

    /**
     * If {@link #mRowIdColumnIndex} is not -1 this contains contains the value of
     * the column at {@link #mRowIdColumnIndex} for the current row this cursor is
     * pointing at.
     */
    protected Long mCurrentRowID;
    protected DataAbilityHelper mContentResolver;
    protected boolean mClosed = false;
    private Uri mNotifyUri;
    private List<Uri> mNotifyUriList;
    private IDataAbilityObserver mSelfObserver;
    final private Object mSelfObserverLock = new Object();
    private boolean mSelfObserverRegistered;
}

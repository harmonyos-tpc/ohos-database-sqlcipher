/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sqlcipher;

import ohos.data.resultset.ResultSet;

/**
 * Extension of ohos.data.resultset.ResultSetWrapper to support getColumnTypeForIndex().
 */
public class CursorWrapper extends ohos.data.resultset.ResultSetWrapper implements ohos.data.resultset.ResultSet {

    private final ResultSet mCursor;

    public CursorWrapper(ResultSet cursor) {
        super(cursor);
        mCursor = cursor;
    }

    public ColumnType getColumnTypeForIndex(int columnIndex) {
        return mCursor.getColumnTypeForIndex(columnIndex);
    }

    public ResultSet getResultSet() {
        return mCursor;
    }
}



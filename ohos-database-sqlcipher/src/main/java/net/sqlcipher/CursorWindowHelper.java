package net.sqlcipher;

import net.sqlcipher.database.SQLiteClosableHos;

public class CursorWindowHelper extends SQLiteClosableHos {

    @Override
    protected void onAllReferencesReleased() {
        //no implementation can be provided now , native dependency on dispose();
    }
}

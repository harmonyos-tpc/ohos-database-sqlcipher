/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.sqlcipher;

import ohos.rpc.IRemoteObject;
import ohos.rpc.MessageOption;
import ohos.rpc.MessageParcel;
import ohos.rpc.RemoteObject;
import ohos.utils.BasePacMap;
import ohos.utils.PacMap;

import ohos.rpc.RemoteException;
import ohos.utils.Parcel;

import java.util.HashMap;
import java.util.Map;

/**
 * Native implementation of the bulk cursor. This is only for use in implementing
 * IPC, application code should use the Cursor interface.
 *
 * {@hide}
 */
public abstract class BulkCursorNative extends RemoteObject implements IBulkCursor
{
    public BulkCursorNative()
    {
        super(null);
        attachLocalInterface(this, descriptor);
    }

    /**
     * Cast a Binder object into a content resolver interface, generating
     * a proxy if needed.
     * @param obj IRemoteObject
     * @return IBulkCursor
     */
    static public IBulkCursor asInterface(IRemoteObject obj)
    {
        if (obj == null) {
            return null;
        }
        IBulkCursor in = (IBulkCursor)obj.queryLocalInterface(descriptor);
        if (in != null) {
            return in;
        }

        return new BulkCursorProxy(obj);
    }

    @Override
    public boolean onRemoteRequest(int code, MessageParcel data, MessageParcel reply, MessageOption flags)
        throws RemoteException {
        try {
            switch (code) {
                case GET_CURSOR_WINDOW_TRANSACTION: {
                    data.readInterfaceToken();
                    int startPos = data.readInt();
                    CursorWindow window = getWindow(startPos);
                    if (window == null) {
                        reply.writeInt(0);
                        return true;
                    }
                    reply.writeNoException();
                    reply.writeInt(1);
                    window.marshalling(reply, 0);
                    return true;
                }

                case COUNT_TRANSACTION: {
                    data.readInterfaceToken();
                    int count = count();
                    reply.writeNoException();
                    reply.writeInt(count);
                    return true;
                }

                case GET_COLUMN_NAMES_TRANSACTION: {
                    data.readInterfaceToken();
                    String[] columnNames = getColumnNames();
                    reply.writeNoException();
                    reply.writeInt(columnNames.length);
                    int length = columnNames.length;
                    for (int i = 0; i < length; i++) {
                        reply.writeString(columnNames[i]);
                    }
                    return true;
                }

                case DEACTIVATE_TRANSACTION: {
                    data.readInterfaceToken();
                    deactivate();
                    reply.writeNoException();
                    return true;
                }

                case CLOSE_TRANSACTION: {
                    data.readInterfaceToken();
                    close();
                    reply.writeNoException();
                    return true;
                }

                case REQUERY_TRANSACTION: {
                    data.readInterfaceToken();
                  /*  IContentObserver observer =
                        IContentObserver.Stub.asInterface(data.readRemoteObject());
                    CursorWindow window = CursorWindow.CREATOR.createFromParcel(data);
                    int count = requery(observer, window);
                    reply.writeNoException();
                    reply.writeInt(count); // TODO AIDL
                    reply.writeParcelable(getExtras());*/
                    return true;
                }

                case UPDATE_ROWS_TRANSACTION: {
                    data.readInterfaceToken();
                    // TODO - what ClassLoader should be passed to readHashMap?
                    // TODO - switch to Bundle
                    HashMap<Long, Map<String, Object>> values = (HashMap<Long, Map<String, Object>>)data.readMap();
                    boolean result = updateRows(values);
                    reply.writeNoException();
                    reply.writeInt((result == true ? 1 : 0));
                    return true;
                }

                case DELETE_ROW_TRANSACTION: {
                    data.readInterfaceToken();
                    int position = data.readInt();
                    boolean result = deleteRow(position);
                    reply.writeNoException();
                    reply.writeInt((result == true ? 1 : 0));
                    return true;
                }

                case ON_MOVE_TRANSACTION: {
                    data.readInterfaceToken();
                    int position = data.readInt();
                    onMove(position);
                    reply.writeNoException();
                    return true;
                }

                case WANTS_ON_MOVE_TRANSACTION: {
                    data.readInterfaceToken();
                    boolean result = getWantsAllOnMoveCalls();
                    reply.writeNoException();
                    reply.writeInt(result ? 1 : 0);
                    return true;
                }

                case GET_EXTRAS_TRANSACTION: {
                    data.readInterfaceToken();
                    PacMap extras = getExtras();
                    reply.writeNoException();
                    reply.writeSequenceable(extras);
                    return true;
                }

                case RESPOND_TRANSACTION: {
                    data.readInterfaceToken();
                    PacMap extras = data.createSequenceable(getClass().getClassLoader());
                    PacMap returnExtras = respond(extras);
                    reply.writeNoException();
                    reply.writeSequenceable(returnExtras);
                    return true;
                }
                /*default:
                    throw new IllegalStateException("Unexpected value: " + code);*/
            }
        } catch (Exception e) {
            DatabaseUtils.writeExceptionToParcel(reply, e);
            return true;
        }
        return super.onRemoteRequest(code, data, reply, flags); //TODO today
    }

    public IRemoteObject asObject()
    {
        return this;
    }
}

final class BulkCursorProxy implements IBulkCursor {
    private IRemoteObject mRemote;
    private PacMap mExtras;

    public BulkCursorProxy(IRemoteObject remote)
    {
        mRemote = remote;
        mExtras = null;
    }

    public IRemoteObject asObject()
    {
        return mRemote;
    }

    public CursorWindow getWindow(int startPos) throws RemoteException
    {
        Parcel data = Parcel.create();
        Parcel reply = Parcel.create();

        ((MessageParcel)data).writeInterfaceToken(descriptor);

        data.writeInt(startPos);

        mRemote.sendRequest(GET_CURSOR_WINDOW_TRANSACTION, (MessageParcel) data, (MessageParcel)reply, new MessageOption(0));//todo

        DatabaseUtils.readExceptionFromParcel(reply); //todo

        CursorWindow window = null;
        if (reply.readInt() == 1) {
            window = CursorWindow.createFromParcel(reply);
        }

        data.reclaim();
        reply.reclaim();

        return window;
    }

    public void onMove(int position) throws RemoteException {
        Parcel data = Parcel.create();
        Parcel reply = Parcel.create();

        ((MessageParcel)data).writeInterfaceToken(descriptor);

        data.writeInt(position);

        mRemote.sendRequest(ON_MOVE_TRANSACTION, (MessageParcel)data, (MessageParcel)reply, new MessageOption(0));

        DatabaseUtils.readExceptionFromParcel(reply);

        data.reclaim();
        reply.reclaim();
    }

    public int count() throws RemoteException
    {
        Parcel data = Parcel.create();
        Parcel reply = Parcel.create();

        ((MessageParcel)data).writeInterfaceToken(descriptor);

        boolean result = mRemote.sendRequest(COUNT_TRANSACTION, (MessageParcel)data, (MessageParcel)reply, new MessageOption(0));

        DatabaseUtils.readExceptionFromParcel(reply);

        int count;
        if (result == false) {
            count = -1;
        } else {
            count = reply.readInt();
        }
        data.reclaim();
        reply.reclaim();
        return count;
    }

    public String[] getColumnNames() throws RemoteException
    {
        Parcel data = Parcel.create();
        Parcel reply = Parcel.create();

        ((MessageParcel)data).writeInterfaceToken(descriptor);

        mRemote.sendRequest(GET_COLUMN_NAMES_TRANSACTION, (MessageParcel)data,(MessageParcel) reply, new MessageOption(0));

        DatabaseUtils.readExceptionFromParcel(reply);

        String[] columnNames = null;
        int numColumns = reply.readInt();
        columnNames = new String[numColumns];
        for (int i = 0; i < numColumns; i++) {
            columnNames[i] = reply.readString();
        }

        data.reclaim();
        reply.reclaim();
        return columnNames;
    }

    public void deactivate() throws RemoteException
    {
        Parcel data = Parcel.create();
        Parcel reply = Parcel.create();

        ((MessageParcel)data).writeInterfaceToken(descriptor);

        mRemote.sendRequest(DEACTIVATE_TRANSACTION, (MessageParcel)data, (MessageParcel)reply, new MessageOption(0));
        DatabaseUtils.readExceptionFromParcel(reply);

        data.reclaim();
        reply.reclaim();
    }

    public void close() throws RemoteException
    {
        Parcel data = Parcel.create();
        Parcel reply = Parcel.create();

        ((MessageParcel)data).writeInterfaceToken(descriptor);

        mRemote.sendRequest(CLOSE_TRANSACTION, (MessageParcel)data, (MessageParcel)reply, new MessageOption(0));
        DatabaseUtils.readExceptionFromParcel(reply);

        data.reclaim();
        reply.reclaim();
    }

    /*public int requery(IContentObserver observer, CursorWindow window) throws RemoteException {
        Parcel data = Parcel.create();
        Parcel reply = Parcel.create();

        ((MessageParcel)data).writeInterfaceToken(descriptor);

       // ((MessageParcel)data).writeRemoteObject(observer);//todo AIDL
        window.marshalling(data, 0);

        boolean result = mRemote.sendRequest(REQUERY_TRANSACTION, (MessageParcel)data, (MessageParcel)reply, new MessageOption(0));

        DatabaseUtils.readExceptionFromParcel(reply);

        int count;
        if (!result) {
            count = -1;
        } else {
            count = reply.readInt();
            mExtras = reply.createSequenceable(getClass().getClassLoader());
        }

        data.reclaim();
        reply.reclaim();

        return count;
    }*/

    public boolean updateRows(Map values) throws RemoteException
    {
        Parcel data = Parcel.create();
        Parcel reply = Parcel.create();

        ((MessageParcel)data).writeInterfaceToken(descriptor);

        data.writeMap(values);

        mRemote.sendRequest(UPDATE_ROWS_TRANSACTION, (MessageParcel)data,(MessageParcel) reply, new MessageOption(0));

        DatabaseUtils.readExceptionFromParcel(reply);

        boolean result = (reply.readInt() == 1);

        data.reclaim();
        reply.reclaim();

        return result;
    }

    public boolean deleteRow(int position) throws RemoteException
    {
        Parcel data = Parcel.create();
        Parcel reply = Parcel.create();

        ((MessageParcel)data).writeInterfaceToken(descriptor);

        data.writeInt(position);

        mRemote.sendRequest(DELETE_ROW_TRANSACTION, (MessageParcel)data, (MessageParcel)reply, new MessageOption(0));

        DatabaseUtils.readExceptionFromParcel(reply);

        boolean result = (reply.readInt() == 1 ? true : false);

        data.reclaim();
        reply.reclaim();

        return result;
    }

    public boolean getWantsAllOnMoveCalls() throws RemoteException {
        Parcel data = Parcel.create();
        Parcel reply = Parcel.create();

        ((MessageParcel)data).writeInterfaceToken(descriptor);

        mRemote.sendRequest(WANTS_ON_MOVE_TRANSACTION, (MessageParcel)data, (MessageParcel)reply, new MessageOption(0));

        DatabaseUtils.readExceptionFromParcel(reply);

        int result = reply.readInt();
        data.reclaim();
        reply.reclaim();
        return result != 0;
    }

    public PacMap getExtras() throws RemoteException {
        if (mExtras == null) {
            Parcel data = Parcel.create();
            Parcel reply = Parcel.create();

            ((MessageParcel)data).writeInterfaceToken(descriptor);

            mRemote.sendRequest(GET_EXTRAS_TRANSACTION, (MessageParcel)data, (MessageParcel)reply, new MessageOption(0));

            DatabaseUtils.readExceptionFromParcel(reply);
            reply.addAppClassLoader(ClassLoader.getSystemClassLoader());
            mExtras = reply.createSequenceable(getClass().getClassLoader());
            data.reclaim();
            reply.reclaim();
        }
        return mExtras;
    }

    public PacMap respond(PacMap extras) throws RemoteException {
        Parcel data = Parcel.create();
        Parcel reply = Parcel.create();

        ((MessageParcel)data).writeInterfaceToken(descriptor);

        data.writeSequenceable(extras);

        mRemote.sendRequest(RESPOND_TRANSACTION, (MessageParcel)data, (MessageParcel)reply, new MessageOption(0));

        DatabaseUtils.readExceptionFromParcel(reply);

        PacMap returnExtras = reply.createSequenceable(getClass().getClassLoader());
        data.reclaim();
        reply.reclaim();
        return returnExtras;
    }
}



package net.sqlcipher.database;

/**
 * An interface to perform pre and post key operations against a database.
 */
public interface SQLiteDatabaseHook {
    /**
     * Called before opening the database for Hos.
     * @param database database
     */
    void preKey(SQLiteDatabase database);
    /**
     * Called immediately after opening the database for Hos.
     * @param database database
     */
    void postKey(SQLiteDatabase database);
}

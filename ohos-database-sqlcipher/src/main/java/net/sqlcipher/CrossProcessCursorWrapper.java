package net.sqlcipher;

import ohos.data.resultset.ResultSet;
import ohos.data.resultset.SharedBlock;
import ohos.data.resultset.SharedResultSet;

public class CrossProcessCursorWrapper extends CursorWrapper implements SharedResultSet {

    public CrossProcessCursorWrapper(ResultSet cursor) {
        super(cursor);
    }

    @Override
    public SharedBlock getBlock() {
        return null;
    }

    @Override
    public void fillBlock(int position, SharedBlock window) {
        DatabaseUtils.cursorFillWindow(this, position, window);
    }

    @Override
    public boolean onGo(int oldPosition, int newPosition) {
        return true;
    }
}

